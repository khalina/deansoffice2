/**
 * View Models used by Spring MVC REST controllers.
 */
package ua.kharkov.univer.deansoffice.web.rest.vm;

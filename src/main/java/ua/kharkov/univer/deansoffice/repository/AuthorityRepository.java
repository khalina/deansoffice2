package ua.kharkov.univer.deansoffice.repository;

import ua.kharkov.univer.deansoffice.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
